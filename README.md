# Readme for j2c
This package exists because I wanted to convert json to csv using python. Some hospitals use json as the "machine-readable" format for the hospital pricing transparency report.

## Upstream
None

## Alternatives
I am sure there are plenty of ways to convert json to csv, including even in python.

## How to use
Run `./json-to-csv.py --help` to learn how to use the cli.

The j2c file is the library, should anybody need that.

## Dependencies
Python3

## References
### Hospital pricing transparency law
[https://www.cms.gov/hospital-price-transparency/hospitals](https://www.cms.gov/hospital-price-transparency/hospitals)
[https://www.federalregister.gov/documents/2019/11/27/2019-24931/medicare-and-medicaid-programs-cy-2020-hospital-outpatient-pps-policy-changes-and-payment-rates-and#h-84](https://www.federalregister.gov/documents/2019/11/27/2019-24931/medicare-and-medicaid-programs-cy-2020-hospital-outpatient-pps-policy-changes-and-payment-rates-and#h-84)

## Differences from upstream
None
