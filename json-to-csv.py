#!/usr/bin/env python3
# File: json-to-csv.py
# Location: https://gitlab.com/bgstack15/j2c
# Author: bgstack15
# Startdate: 2021-01-16
# SPDX-License-Identifier: CC-BY-SA-4.0
# Title: Json to CSV cli utility
# Purpose: Front-end logic and argument parser
# History:
# Usage:
# Reference:
# Improve:
# Dependencies:
#    j2c.py in this package
# Documentation:
from j2c import *
import argparse
debug = 0

parser = argparse.ArgumentParser()
parser.add_argument("-d","--debug", nargs='?', default=0, type=int, choices=range(0,11),help="Set debug level.")
parser.add_argument("-i","--infile", help="Json file to convert")
parser.add_argument("-o","--outfile", help="Csv file to output")
args = parser.parse_args()

debug = args.debug if args.debug != None else 0
if args.infile is None:
   print("Need -i infile! Aborted",file=sys.stderr)
   sys.exit(1)
else:
   jsonfile = args.infile

if args.outfile is None:
   outfile = "stdout"
else:
   outfile = args.outfile

convert_json_to_csv(jsonfile,outfile,debug=debug)
